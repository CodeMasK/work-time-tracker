from django.contrib.auth.models import Group
from faker import Factory  # pylint: disable=import-error
from guardian.shortcuts import assign_perm  # pylint: disable=import-error
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from apps.projects.models import Project
from apps.users.auth import serializers
from apps.users.models import User


class ProjectTestCase(APITestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.client = APIClient()
        extra_data = {}

        fake = Factory.create()
        fake_user = fake.simple_profile()
        cls.fake_email = fake_user["mail"]
        cls.fake_password = fake.password()

        extra_data["first_name"] = fake_user["name"].split(" ")[0]
        extra_data["last_name"] = fake_user["name"].split(" ")[1]
        extra_data["username"] = fake_user["username"]

        # create test user
        cls.user = User.objects.create_user(
            cls.fake_email, cls.fake_password, **extra_data
        )
        data = serializers.AuthSerializer(cls.user).data
        cls.user_id = data["id"]
        cls.token = data["auth_token"]

        # create test projects
        project_1 = Project.objects.create(
            title="Work Project 1",
            description="Testing Work Project 1",
            creator_id=cls.user.id,
        )
        project_2 = Project.objects.create(
            title="Work Project 2",
            description="Testing Work Project 2",
            creator_id=cls.user.id,
        )
        cls.project_id_1 = project_1.id
        cls.project_id_2 = project_2.id

        # add project to creator's instance
        cls.user.projects.add(project_1)
        cls.user.projects.add(project_2)

        # create project groups and permit group members to view
        project_group_1 = Group.objects.create(name=project_1.title)
        assign_perm("view_project", project_group_1, project_1)

        project_group_2 = Group.objects.create(name=project_2.title)
        assign_perm("view_project", project_group_2, project_2)

        # add test user to project groups
        cls.user.groups.add(project_group_1)
        cls.user.groups.add(project_group_2)

        # permit project creator to update and delete project bring t
        assign_perm("change_project", cls.user, project_1)
        assign_perm("delete_project", cls.user, project_1)

        assign_perm("change_project", cls.user, project_2)
        assign_perm("delete_project", cls.user, project_2)

    def test_retrieve_single_project(self):
        """
        Test to Retrieve a single project with ID
        """
        url = f"/api/project/{self.project_id_1}/"
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.get(url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_all_projects(self):
        """
        Test to Retrieve projects that are accessible user
        """
        url = "/api/project/"
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.get(url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_project(self):
        """
        Test to Create project
        """
        url = "/api/project/"
        data = {
            "title": "My Project",
            "description": "My Project used to track my work progress",
            "creator_id": self.user_id,
        }
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_project(self):
        """
        Test to Update project with ID
        """
        url = f"/api/project/{self.project_id_1}/"
        data = {
            "title": "My New Project",
            "description": "My New Project updated from My Project",
        }
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.put(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_project(self):
        """
        Test to Delete project with ID
        """
        url = f"/api/project/{self.project_id_1}/"
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)

        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_add_member(self):
        """
        Test to add member to project
        """
        data = {"user_id": self.user_id, "action": "add"}
        url = f"/api/project/{self.project_id_2}/member/"
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)

        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_remove_member(self):
        """
        Test to remove member from project
        """
        data = {"user_id": self.user_id, "action": "remove"}
        url = f"/api/project/{self.project_id_2}/member/"
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)

        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
