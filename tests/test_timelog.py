from django.contrib.auth.models import Group
from django.utils.dateparse import parse_datetime
from django.utils.timezone import make_aware
from faker import Factory  # pylint: disable=import-error
from guardian.shortcuts import assign_perm  # pylint: disable=import-error
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from apps.projects.models import Project, ProjectTimeLog
from apps.users.auth import serializers
from apps.users.models import User


class ProjectTimeLogTestCase(APITestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.client = APIClient()
        extra_data = {}

        fake = Factory.create()
        fake_user = fake.simple_profile()
        cls.fake_email = fake_user["mail"]
        cls.fake_password = fake.password()

        extra_data["first_name"] = fake_user["name"].split(" ")[0]
        extra_data["last_name"] = fake_user["name"].split(" ")[1]
        extra_data["username"] = fake_user["username"]

        # create test user
        cls.user = User.objects.create_user(
            cls.fake_email, cls.fake_password, **extra_data
        )
        data = serializers.AuthSerializer(cls.user).data
        cls.user_id = data["id"]
        cls.token = data["auth_token"]

        # create test projects
        project_1 = Project.objects.create(
            title="Work Project 1",
            description="Testing Work Project 1",
            creator_id=cls.user.id,
        )
        project_2 = Project.objects.create(
            title="Work Project 2",
            description="Testing Work Project 2",
            creator_id=cls.user.id,
        )
        cls.project_id_1 = project_1.id
        cls.project_id_2 = project_2.id

        # add project to creator's instance
        cls.user.projects.add(project_1)
        cls.user.projects.add(project_2)

        # create project groups and permit group members to view
        project_group_1 = Group.objects.create(name=project_1.title)
        assign_perm("view_project", project_group_1, project_1)

        project_group_2 = Group.objects.create(name=project_2.title)
        assign_perm("view_project", project_group_2, project_2)

        # add test user to project groups
        cls.user.groups.add(project_group_1)
        cls.user.groups.add(project_group_2)

        # permit project creator to update and delete project bring t
        assign_perm("change_project", cls.user, project_1)
        assign_perm("delete_project", cls.user, project_1)

        assign_perm("change_project", cls.user, project_2)
        assign_perm("delete_project", cls.user, project_2)

        # create test time log for user in project 1
        from django.utils import timezone

        tz = timezone.get_current_timezone()
        parsed_starttime = parse_datetime("2022-03-18 12:30:00")
        parsed_starttime.replace(tzinfo=tz)

        parsed_endtime = parse_datetime("2022-03-19 14:08:35")
        parsed_endtime.replace(tzinfo=tz)

        data = {
            "project": project_1,
            "user_id": cls.user_id,
            "log_date": "2022-03-18",
            "log_note": "log time for test case in project 1",
            "start_time": make_aware(parsed_starttime),
            "end_time": make_aware(parsed_endtime),
        }
        cls.timelog_1 = ProjectTimeLog.objects.create(**data)
        cls.timelog_1_id = cls.timelog_1.id

        # grant project group the permission to view timelog
        assign_perm("view_projecttimelog", project_group_1, cls.timelog_1)
        # grant creator the sole permission to update and delete timelog
        assign_perm("change_projecttimelog", cls.user, cls.timelog_1)
        assign_perm("delete_projecttimelog", cls.user, cls.timelog_1)

    def test_create_time_log(self):
        """
        Test to create log time
        """
        url = "/api/project/timelog/"
        data = {
            "project": self.project_id_1,
            "user_id": self.user_id,
            "log_date": "2022-03-19",
            "log_note": "Tested the work log time tracker",
            "start_time": "2022-03-19 08:00:00",
            "end_time": "2022-03-19 10:15:00",
        }
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_time_log(self):
        """
        Test to Update project time log with ID
        """
        url = f"/api/project/timelog/{self.timelog_1_id}/"
        data = {
            "project": self.project_id_1,
            "user_id": self.user_id,
            "log_date": "2022-03-19",
            "log_note": "work log time tracker",
            "start_time": "2022-03-18 12:10:00",
            "end_time": "2022-03-18 13:05:00",
        }
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.put(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_single_time_log(self):
        """
        Test to retrieve log time instance with ID
        """
        url = f"/api/project/timelog/{self.timelog_1_id}/"
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_project_time_logs(self):
        """
        Test to Retrieve all log time for a project
        """
        url = f"/api/project/timelog/?project={self.project_id_1}"
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_time_log(self):
        """
        Test to Delete project time log with ID
        """
        url = f"/api/project/timelog/{self.timelog_1_id}/"
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)

        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
