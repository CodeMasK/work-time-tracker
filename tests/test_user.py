from faker import Factory  # pylint: disable=import-error
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from apps.users.auth import serializers
from apps.users.models import User


class UserTestCase(APITestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.client = APIClient()
        extra_data = {}

        fake = Factory.create()
        fake_user = fake.simple_profile()
        cls.fake_email = fake_user["mail"]
        cls.fake_password = fake.password()

        extra_data["first_name"] = fake_user["name"].split(" ")[0]
        extra_data["last_name"] = fake_user["name"].split(" ")[1]
        extra_data["username"] = fake_user["username"]

        # create test user
        user = User.objects.create_user(cls.fake_email, cls.fake_password, **extra_data)
        data = serializers.AuthSerializer(user).data
        cls.user_id = data["id"]
        cls.token = data["auth_token"]

    def test_retrieve_user(self):
        """
        Test to Retrieve user profile
        """
        url = "/api/user/"
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.get(url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        data = response.json()

        self.assertEqual(len(data), 1)
        self.assertEqual(data[0]["email"], self.fake_email)

    def test_create_user(self):
        """
        Test to Create user profile
        """
        url = "/api/user/"
        data = {
            "username": "damilola",
            "first_name": "Damilola",
            "last_name": "Odeyemi",
            "email": "damilola@outlook.com",
            "password": "Code@123456",
        }
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response_data = response.json()
        self.assertEqual(response_data["username"], data["username"])

    def test_update_user(self):
        """
        Test to Update user profile
        """
        url = f"/api/user/{self.user_id}/"
        data = {
            "username": "matt@outlook.com",
            "first_name": "Matt",
            "last_name": "Odeyemi",
            "email": "matt@utlook.com",
        }
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.put(url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["username"], data["username"])
        self.assertEqual(response.data["first_name"], data["first_name"])
        self.assertEqual(response.data["last_name"], data["last_name"])
        self.assertEqual(response.data["email"], data["email"])

    def test_password_change(self):
        """
        Test to Change user password
        """
        password_change_url = "/api/user/password-change/"
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)

        data = {}
        data["current_password"] = self.fake_password
        data["new_password"] = self.fake_password

        response = self.client.post(password_change_url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # confirm password change
        login_url = "/api/auth/login/"
        data = {"email": self.fake_email, "password": data["new_password"]}
        response = self.client.post(login_url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_user(self):
        """
        Test to Delete user profile
        """
        url = f"/api/user/{self.user_id}/"
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)

        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
