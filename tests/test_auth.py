from faker import Factory  # pylint: disable=import-error
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from apps.users.auth import serializers
from apps.users.models import User


class AuthTestCase(APITestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.client = APIClient()
        extra_data = {}

        cls.fake = Factory.create()
        cls.fake_user = cls.fake.simple_profile()
        cls.fake_email = cls.fake_user["mail"]
        cls.fake_password = cls.fake.password()

        extra_data["first_name"] = cls.fake_user["name"].split(" ")[0]
        extra_data["last_name"] = cls.fake_user["name"].split(" ")[1]
        extra_data["username"] = cls.fake_user["username"]

        # create test user
        user = User.objects.create_user(cls.fake_email, cls.fake_password, **extra_data)
        data = serializers.AuthSerializer(user).data
        cls.token = data["auth_token"]

    def test_login(self):
        """
        User Login Test Case
        """
        url = "/api/auth/login/"
        data = {"email": self.fake_email, "password": self.fake_password}
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_logout(self):
        """
        User Logout Test Case
        """
        url = "/api/auth/logout/"
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token)
        response = self.client.post(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_register(self):
        """
        User Registration Test Case
        """
        url = "/api/auth/register/"
        fake_user = self.fake.simple_profile()
        email = fake_user["mail"]
        password = self.fake.password()
        first_name = fake_user["name"].split(" ")[0]
        last_name = fake_user["name"].split(" ")[1]

        data = {
            "email": email,
            "password": password,
            "first_name": first_name,
            "last_name": last_name,
        }
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
