
# Work Time Tracker Service

A multi-user and multi-project work time tracking application using Django and Django Rest Framework

## API Reference

http://127.0.0.1:8000/redoc/


## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`DEBUG_MODE`

`ALLOWED_HOSTS`

`SECRET_KEY`

## Running Tests

To run tests and generate coverage report, run the following command

```bash
  sh test_coverage.sh
```


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/CodeMasK/work-time-tracker.git
```

Go to the project directory

```bash
  cd work-time-tracker
```

Create virtual environment
```bash
  python3 -m venv .venv
```

Activate virtual environment
```bash
  source .venv/bin/activate
```

Install dependencies

```bash
  pip install -r requirements.txt
```

Start the server

```bash
  sh start-dev.sh
```


## Authors

- [@codemask](mattode@outlook.com)
