class CustomException(Exception):
    """Custom Exception"""

    def __init__(self, err: str):
        super().__init__(self)
        self.error = err

    def __str__(self) -> str:
        return self.error
