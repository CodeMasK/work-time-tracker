from django.contrib.auth import password_validation
from rest_framework import serializers

from .models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = "__all__"
        read_only_fields = [
            "id",
            "date_joined",
            "created_at",
            "updated_at",
            "is_active",
            "is_staff",
            "is_superuser",
        ]
        extra_kwargs = {"password": {"write_only": True}}


class PasswordChangeSerializer(serializers.Serializer):
    """
    User Password Change serializer
    """

    current_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

    def validate_current_password(self, value):
        """
        Checks inputted password to match existing password
        """
        if not self.context["request"].user.check_password(value):
            raise serializers.ValidationError("Current password does not match")
        return value

    def validate_new_password(self, value):
        """
        Validates new password entry to meet standard requirements
        """
        password_validation.validate_password(value)
        return value
