from rest_framework import permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from .models import User
from .serializers import PasswordChangeSerializer, UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    pagination_class = None

    def get_permissions(self):
        return (permissions.IsAuthenticated(),)

    def get_queryset(self):
        if self.request.user.is_superuser:
            queryset = User.objects.all()
        else:
            queryset = User.objects.filter(id=self.request.user.id)

        return queryset

    def update(self, request, *args, **kwargs):
        """
        Update a user instance
        """
        partial = True
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        return Response(
            {"message": "Not allowed to delete profile. Contact Admin"},
            status=status.HTTP_403_FORBIDDEN,
        )

    @action(methods=["POST"], detail=False, url_path="password-change")
    def password_change(self, request):
        """
        Change password for authenticated users
        """
        serializer = PasswordChangeSerializer(
            data=request.data, context={"request": request}
        )
        serializer.is_valid(raise_exception=True)
        request.user.set_password(serializer.validated_data["new_password"])
        request.user.save()
        return Response(status=status.HTTP_200_OK)
