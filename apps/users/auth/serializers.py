from django.contrib.auth import password_validation
from django.contrib.auth.models import BaseUserManager
from rest_framework import serializers
from rest_framework.authtoken.models import Token

from apps.users.models import User


class AuthSerializer(serializers.ModelSerializer):
    auth_token = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = (
            "id",
            "email",
            "first_name",
            "last_name",
            "is_active",
            "is_staff",
            "auth_token",
            "created_at",
        )
        read_only_fields = ("id", "is_active", "is_staff", "created_at")

    def get_auth_token(self, obj) -> str:
        """
        Retrieve user authentication key
        """
        token, _ = Token.objects.get_or_create(user=obj)
        return token.key


class RegisterSerializer(serializers.ModelSerializer):
    """
    User Registration serializer to format and validate input/output data for sign-up
    """

    class Meta:
        model = User
        fields = ("id", "email", "password", "first_name", "last_name")

    def validate_email(self, value) -> str:
        """
        Validates the email is not taken and normalizes it for conformity
        """
        user = User.objects.filter(email=value)
        if user:
            raise serializers.ValidationError("Email is already taken")

        return BaseUserManager.normalize_email(value)

    def validate_password(self, value) -> str:
        """
        Validates password to ensure it meets standard requirements
        """
        password_validation.validate_password(value)
        return value


class LoginSerializer(serializers.Serializer):
    """
    User Login serializer to format and validate input/output data for sign-in
    """

    email = serializers.CharField(max_length=300, required=True)
    password = serializers.CharField(required=True)


class EmptySerializer(serializers.Serializer):
    """
    Empty serializer for empty data validations
    """
