from django.contrib.auth import logout
from django.core.exceptions import ImproperlyConfigured
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from apps.users.auth import serializers
from apps.users.helpers import create_user_account, get_and_authenticate_user


class AuthViewSet(viewsets.GenericViewSet):
    queryset = []
    permission_classes = [
        AllowAny,
    ]
    serializer_classes = {
        "login": serializers.LoginSerializer,
        "register": serializers.RegisterSerializer,
        "logout": serializers.EmptySerializer,
    }

    def get_serializer_class(self):
        if not isinstance(self.serializer_classes, dict):
            raise ImproperlyConfigured("serializer_classes should be a dict mapping.")

        if self.action in self.serializer_classes.keys():
            return self.serializer_classes[self.action]

        return super().get_serializer_class()

    @action(
        methods=[
            "POST",
        ],
        detail=False,
    )
    def login(self, request):
        """
        Login method for registered users with email and password
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = get_and_authenticate_user(**serializer.validated_data)
        data = serializers.AuthSerializer(user).data
        return Response(data=data, status=status.HTTP_200_OK)

    @action(
        methods=[
            "POST",
        ],
        detail=False,
    )
    def register(self, request):
        """
        New User Registration method
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = create_user_account(**serializer.validated_data)
        data = serializers.AuthSerializer(user).data
        return Response(data=data, status=status.HTTP_201_CREATED)

    @action(
        methods=[
            "POST",
        ],
        detail=False,
    )
    def logout(self, request):
        """
        Logout method for authenticated and anonymous users
        """
        logout(request)
        data = {"success": "User sucessfully logged out"}
        return Response(data=data, status=status.HTTP_200_OK)
