from django.contrib.auth import authenticate
from rest_framework import serializers

from .models import User


def get_and_authenticate_user(email, password):
    """
    Util function to fetch and authenticate a user profile
    """
    user = authenticate(username=email, password=password)
    if user is None:
        raise serializers.ValidationError(
            "Invalid username/password. Please try again!"
        )

    return user


def create_user_account(email, password, first_name, last_name, **extra_fields):
    """
    Util function to create a user profile
    """
    user = User.objects.create_user(
        email=email,
        password=password,
        first_name=first_name,
        last_name=last_name,
        **extra_fields
    )

    return user


def get_user_by_id(user_id):
    """
    Retrieve user instance with ID
    """
    try:
        user = User.objects.get(id=user_id)
        return user
    except User.DoesNotExist:
        return None
