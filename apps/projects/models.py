from django.db import models


class Project(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    is_archived = models.BooleanField(default=False)
    creator_id = models.CharField(max_length=64)

    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        ordering = ["-id"]
        verbose_name_plural = "Projects"

    def __str__(self):
        return str(self.title)


class ProjectTimeLog(models.Model):
    project = models.ForeignKey(
        Project, related_name="timelogs", on_delete=models.CASCADE
    )
    user_id = models.CharField(max_length=64)
    log_date = models.DateField(blank=True)
    log_note = models.TextField(blank=True, null=True)

    start_time = models.DateTimeField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        ordering = ["-log_date", "-id"]
