from rest_framework import serializers

from apps.users.helpers import get_user_by_id

from .models import Project, ProjectTimeLog


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = (
            "id",
            "title",
            "description",
            "creator_id",
            "is_archived",
        )
        read_only_fields = [
            "id",
            "created_at",
        ]

    def get_queryset(self):
        """
        Retrieve Queryset
        """
        queryset = super().get_queryset()
        return queryset.filter(is_archived=False)


class ProjectTimeLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectTimeLog
        fields = (
            "id",
            "project",
            "user_id",
            "log_date",
            "log_note",
            "start_time",
            "end_time",
            "created_at",
        )
        read_only_fields = [
            "id",
            "created_at",
        ]


class ProjectMembershipSerializer(serializers.Serializer):
    """
    Add/Remove member serializer
    """

    user_id = serializers.CharField(required=True)
    action = serializers.CharField(required=True)

    def validate_user_id(self, value):
        """
        Checks if ID belongs to a user
        """
        if not get_user_by_id(value):
            raise serializers.ValidationError("User ID does not exist")
        return value

    def validate_action(self, value):
        """
        Validates if action == add | remove
        """
        if value not in ("add", "remove"):
            raise serializers.ValidationError("Invalid action")
        return value
