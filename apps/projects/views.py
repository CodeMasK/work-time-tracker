from django.contrib.auth.models import AnonymousUser, Group
from django.core.exceptions import ObjectDoesNotExist
from django_filters.rest_framework import (  # pylint: disable=import-error
    DjangoFilterBackend,
)
from guardian.shortcuts import assign_perm  # pylint: disable=import-error
from rest_framework import permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound, PermissionDenied
from rest_framework.filters import OrderingFilter
from rest_framework.response import Response

from apps.users.helpers import get_user_by_id

from .models import Project, ProjectTimeLog
from .serializers import (
    ProjectMembershipSerializer,
    ProjectSerializer,
    ProjectTimeLogSerializer,
)


class ProjectViewSet(viewsets.ModelViewSet):
    queryset = Project.objects.filter(is_archived=False)
    serializer_class = ProjectSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        """
        Returns the list of projects that is specific to the user
        """
        if self.request.user.is_superuser:
            return Project.objects.all()

        if isinstance(self.request.user, AnonymousUser):
            # return empty query set for anonymous users
            return Project.objects.none()

        # get projects that user is only a member
        return self.request.user.projects.all()

    def get_object(self):
        """
        Returns the project instance the view is handling.
        """
        try:
            queryset = self.filter_queryset(self.get_queryset())

            # Perform the lookup filtering.
            lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
            filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
            # obj = get_object_or_404(queryset, **filter_kwargs)
            obj = queryset.get(**filter_kwargs)

            # May raise a permission denied
            self.check_object_permissions(self.request, obj)

            return obj
        except (ObjectDoesNotExist,) as err:
            raise NotFound("Project Not Found", status.HTTP_404_NOT_FOUND) from err

    def check_object_permissions(self, request, obj, **kwargs):
        """
        Check if the request should be permitted for a given object.
        Raises an appropriate exception if the request is not permitted.
        """
        user = request.user
        action = kwargs.get("action")
        if action:
            if user.has_perm(action, obj) is False:
                raise PermissionDenied("Access Denied", status.HTTP_403_FORBIDDEN)
            return True

        return super().check_object_permissions(request, obj)

    def perform_create(self, serializer):
        return serializer.save()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        obj = self.perform_create(serializer)

        # add project to creator's instance
        user = request.user
        user.projects.add(obj)

        # create project group
        group = Group.objects.create(name=obj.title)
        # assign group the permission to view
        assign_perm("view_project", group, obj)
        # add group to creator's instance
        user.groups.add(group)

        # grant creator the permissions to update and delete project
        assign_perm("change_project", user, obj)
        assign_perm("delete_project", user, obj)

        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", True)
        obj = self.get_object()
        serializer = self.get_serializer(obj, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)

        # check user permissions for given object
        self.check_object_permissions(request, obj, action="change_project")

        self.perform_update(serializer)
        return Response(serializer.data)

    def perform_destroy(self, obj):
        # check user permissions for given object
        request_ctx = self.request
        self.check_object_permissions(request_ctx, obj, action="delete_project")
        obj.delete()

    def handle_membership(self, action, obj, user) -> None:
        """
        Handles membership add / remove to project
        :: action => add | remove
        :: obj => project instance
        :: user => member instance
        """
        group, _ = Group.objects.get_or_create(name=obj.title)
        if action == "add":
            # add project to user instance
            user.projects.add(obj)
            # add group to user instance
            user.groups.add(group)

        elif action == "remove":
            # remove project from user instance
            user.projects.remove(obj)
            # remove group from user instance
            user.groups.remove(group)

    @action(methods=["POST"], detail=True, url_path="member")
    def project_membership(self, request, pk=None):
        """
        Add or Remove project members
        """
        serializer = ProjectMembershipSerializer(
            data=request.data, context={"request": request}
        )
        serializer.is_valid(raise_exception=True)

        obj = self.get_object()

        user_id = serializer.validated_data["user_id"]
        action = serializer.validated_data["action"]

        # get user instance
        user = get_user_by_id(user_id)

        # check if request executor is project creator
        if request.user.id != int(obj.creator_id):
            raise PermissionDenied(
                "Only Project creators are allowed to add/remove project members",
                status.HTTP_403_FORBIDDEN,
            )

        self.handle_membership(action, obj, user)
        return Response(status=status.HTTP_200_OK)


class ProjectTimeLogViewSet(viewsets.ModelViewSet):
    queryset = ProjectTimeLog.objects.all()
    serializer_class = ProjectTimeLogSerializer
    permission_classes = [permissions.IsAuthenticated]
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ["project", "user_id"]
    ordering_fields = (
        "log_date",
        "project__id",
    )
    ordering = ("-log_date", "-id")

    def get_queryset(self):
        """
        Returns the list of projects that is specific to the user
        """
        if self.request.user.is_superuser:
            return ProjectTimeLog.objects.all()

        # get time log for projects that user is only a member
        if isinstance(self.request.user, AnonymousUser):
            # return empty query set for anonymous users
            projects = Project.objects.none()
        else:
            projects = list(self.request.user.projects.values_list("id", flat=True))

        return ProjectTimeLog.objects.filter(project__in=projects)

    def check_object_permissions(self, request, obj, **kwargs):
        """
        Check if the request should be permitted for a given object.
        Raises an appropriate exception if the request is not permitted.
        """
        user = request.user
        action = kwargs.get("action")
        if action:
            if user.has_perm(action, obj) is False:
                raise PermissionDenied("Access Denied", status.HTTP_403_FORBIDDEN)
            return True

        return super().check_object_permissions(request, obj)

    def perform_create(self, serializer):
        return serializer.save()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        obj = self.perform_create(serializer)

        user_ctx = request.user
        if user_ctx.id != int(serializer.data["user_id"]):
            raise PermissionDenied(
                "Not Allowed to Log Time for other users", status.HTTP_403_FORBIDDEN
            )

        # get project group
        group = Group.objects.get(name=obj.project.title)

        # grant project group the permission to view timelog
        assign_perm("view_projecttimelog", group, obj)

        # grant creator the sole permission to update and delete timelog
        assign_perm("change_projecttimelog", user_ctx, obj)
        assign_perm("delete_projecttimelog", user_ctx, obj)

        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop("partial", True)
        obj = self.get_object()
        serializer = self.get_serializer(obj, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)

        # check user permissions for given object
        self.check_object_permissions(request, obj, action="change_projecttimelog")

        self.perform_update(serializer)
        return Response(serializer.data)

    def perform_destroy(self, obj):
        # check user permissions for given object
        request_ctx = self.request
        self.check_object_permissions(request_ctx, obj, action="delete_projecttimelog")
        obj.delete()
