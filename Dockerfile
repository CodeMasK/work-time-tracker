FROM python:3.10

RUN apt-get update && \
    apt-get install -y && \
    python -m pip install --upgrade pip

COPY . /home
WORKDIR /home

ENV PYTHONPATH=${PYTHONPATH}:${PWD}

RUN pip install -r /home/requirements.txt

EXPOSE 8888

# grants the execute permission to all
RUN chmod +x /home/start-prod.sh
# instruction to set executables that will always run when the container is initiated
ENTRYPOINT /home/start-prod.sh
