"""time_tracker URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from __future__ import annotations

from django.contrib import admin
from django.urls import include, path, re_path
from drf_yasg import openapi  # pylint: disable=import-error
from drf_yasg.views import get_schema_view  # pylint: disable=import-error
from rest_framework import permissions, routers

from apps.projects.views import ProjectTimeLogViewSet, ProjectViewSet
from apps.users.auth.views import AuthViewSet
from apps.users.views import UserViewSet

router = routers.DefaultRouter(trailing_slash=True)
router.register("user", UserViewSet, basename="user")
router.register("auth", AuthViewSet, basename="auth")
router.register("project/timelog", ProjectTimeLogViewSet, basename="project-time-log")
router.register("project", ProjectViewSet, basename="project")

schema_view = get_schema_view(
    openapi.Info(
        title="Work TimeTracker API",
        default_version="v1.0.0",
        description="Work Time Tracker",
        contact=openapi.Contact(email="mattode@outlook.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
)

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", include(router.urls)),
    re_path(
        r"^redoc/$", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"
    ),
]
