python manage.py makemigrations

python manage.py migrate

python -m gunicorn time_tracker.asgi:application -k uvicorn.workers.UvicornWorker -b 127.0.0.1:8000
